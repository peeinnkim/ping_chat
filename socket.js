var express = require('express');
var app = express();
var http = require('http').Server(app);

var io = require('socket.io')(http);

var ejs = require('ejs');

var multer = require('multer');
var path = require('path');

var upload = multer({
  storage: multer.diskStorage({ // storage: 저장과 관련된 정보
    destination: function (req, file, cb) { // destination: 목적지
      cb(null, 'assets/upload/');
    },
    filename: function (req, file, cb) {
      cb(null, 'ping_' + new Date().valueOf() + path.extname(file.originalname));
    }
    // limits: { fileSize: 5 * 1024 * 1024 }
  }),
});

var fs = require('fs');

var bodyParser = require('body-parser');
const { SSL_OP_COOKIE_EXCHANGE } = require('constants');

var curNickname = "";

app.use('/assets', express.static(__dirname + '/assets'));
// app.use('/upload', express.static(__dirname + '/upload'));
app.use(bodyParser.urlencoded({
  extended: true
}));

app.engine('html', ejs.renderFile);
app.get('/', function(req, res){
  res.render(__dirname + '/views/index.ejs');

  console.log('in / GET');
});

// main page
app.get('/ping', function(req, res){
  res.redirect('/');
});

// nickname setting
app.post('/ping', function(req, res){
  console.log('in /ping POST');

  curNickname = req.body.nickname;
  console.log('new user nickname : ' + curNickname);

  res.render(__dirname+'/views/socket_chat.ejs');
});

// file upload
app.post('/upload', upload.single('file'), function(req, res) {
	// console.log( req.file );
  res.send({'result': 'success', 'data': req.file});
});


var curUserList = {};

function max(a){
	alert(a);
}

// socket connected
io.on('connection', function(socket){
	console.log('--------- user connection ---------');
	console.log('Connected ID : ' + socket.id);
  

  // add user to user list
  var nickname = curNickname || socket.id;
  curUserList[socket.id] = nickname;


  // login user
  socket.emit('login', {'nickname': nickname, 'socketId': socket.id, 'userList': curUserList});
  

  // new user
  io.emit('newUser', {'nickname': nickname, 'userList': curUserList});


  // logout user
  socket.on('disconnect', function(data){
    console.log('--------- user disconnection ---------');
    console.log('Disconnected ID : ' + socket.id);  

    delete curUserList[socket.id];
    io.emit('logout', {'userList': curUserList, 'nickname': nickname});
  });


  // send message
  socket.on('sendMsg',function(data){
    console.log('--------- activate send message ---------');
    
    var json_data = JSON.parse(data);
    console.log(json_data);
    
    if(json_data.to == "ALL") {
      console.log('--------- activate to ALL ---------');
      
      socket.broadcast.emit('chatOthers', json_data);
      socket.emit('chatMe', json_data);	
      
    } else {
      console.log('--------- activate to user ---------');
      console.log(json_data.to);
      
      if (json_data.to != undefined) {
        socket.broadcast.to(json_data.to).emit('chatOthers', json_data);
        socket.emit('chatMe', json_data);	
      }
    }
  });
  
  
  // send Image
  socket.on('sendImg', function(data){
    console.log('--------- activate send image ---------');
    
    var json_data = JSON.parse(data);

    if(json_data.to == "ALL") {
      console.log('--------- activate to ALL ---------');
      
      socket.broadcast.emit('imgOthers', json_data);
      socket.emit('imgMe', json_data);	
      
    } else {
      console.log('--------- activate to user ---------');
      console.log(json_data.to);
      
      if (json_data.to != undefined) {
        socket.broadcast.to(json_data.to).emit('imgOthers', json_data);
        socket.emit('imgMe', json_data);	
      }
    }
  });
  
  
  // change user nickname
  socket.on('changeNickname', function(data){
    var json_data= JSON.parse(data);
    var oldNick = json_data.oldNickname;
    var newNick = json_data.newNickname;

    curUserList[socket.id] = newNick;

    var alertMsg = '[' + oldNick + '] 님이 [' + newNick + '] 으로 닉네임을 변경하셨습니다.';
    
    io.emit('nickChange', {'userList': curUserList, 'msg': alertMsg});
    socket.emit('setNewNickname', {'nickname': newNick});
  });  

});

http.listen(3000, function(){
  console.log('listening on *:3000');
  
  fs.mkdir('./assets/upload', function(err){
    if(err) throw err;
    console.log('create new upload directory');
  });

});

http.on('close', function(){
  console.log('--------- server close ---------');
  fs.rmdir('./assets/upload', { recursive: true }, function(err){
    if(err) throw err;
    console.log('delete upload directory');
  });
});

process.on('SIGINT', function() {
  http.close();
});


